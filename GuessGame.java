package game;

/**
 * Created by g15oit18 on 19.09.2017.
 */
public class GuessGame {

    Player player1;
    Player player2;
    Player player3;

    public void startGame() {
        player1 = new Player();
        player2 = new Player();
        player3 = new Player();
        int guessp1 = 0;
        int guessp2 = 0;
        int guessp3 = 0;
        boolean player1IsRight = false;
        boolean player2IsRight = false;
        boolean player3IsRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Я думаю о числе от 0 до 9 ...");
        while (true) {
            System.out.println("Число, которое нужно угадать" + targetNumber);

            player1.guess();
            player2.guess();
            player3.guess();

            guessp1 = player1.number;
            System.out.println("Первый игрок догадался " + guessp1);
            guessp2 = player2.number;
            System.out.println("Второй игрок угадал " + guessp2);
            guessp3 = player3.number;
            System.out.println("Третий игрок догадался " + guessp3);

            if (guessp1 == targetNumber) {
                player1IsRight = true;
            }
            if (guessp2 == targetNumber) {
                player2IsRight = true;
            }
            if (guessp3 == targetNumber) {
                player3IsRight = true;
            }

            if (player1IsRight || player2IsRight || player3IsRight) {
                System.out.println("У нас есть победитель!");
                System.out.println("Один игрок получил это право? " + player1IsRight);
                System.out.println("Второй игрок получил это право? " + player2IsRight);
                System.out.println("Игрок три получил это право? " + player3IsRight);
                System.out.println("Игра окончена");
                break;
            } else {
                System.out.println("Players will have to try again.");
            }
        }
    }
}