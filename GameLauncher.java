package game;

/**
 * Created by g15oit18 on 19.09.2017.
 */
public class GameLauncher {
    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}